REFS: Links to originating documentation - and copies of original sample files

# Add a dynamic NFS provisioner to your Kubernetes cluster
https://developer.ibm.com/tutorials/add-nfs-provisioner-to-icp/
https://s3.us.cloud-object-storage.appdomain.cloud/developer/tutorials/add-nfs-provisioner-to-ICP/static/nfs-deployment-icp.yaml
https://github.com/IBM/deploy-ibm-cloud-private/blob/master/extensions/nfs-deployment.yaml


# Deploy nfs-provisioner on ICP master
https://github.com/IBM/deploy-ibm-cloud-private/blob/master/docs/deploy-nfs-provisioner.md

# Getting the provisioner image
https://github.com/kubernetes-incubator/external-storage/blob/master/nfs/docs/deployment.md

# nfs-provisioner
https://github.com/kubernetes-incubator/external-storage/tree/master/nfs


# Deploy nfs-provisioner on ICP master
https://github.com/IBM/deploy-ibm-cloud-private/blob/master/docs/deploy-nfs-provisioner.md
# nfs-class.yaml nfs-deployment.yaml nfs-test-claim.yaml
https://github.com/IBM/deploy-ibm-cloud-private/tree/master/extensions
https://github.com/IBM/deploy-ibm-cloud-private/blob/master/extensions/nfs-class.yaml
https://github.com/IBM/deploy-ibm-cloud-private/blob/master/extensions/nfs-deployment.yaml
https://github.com/IBM/deploy-ibm-cloud-private/blob/master/extensions/nfs-test-claim.yaml








