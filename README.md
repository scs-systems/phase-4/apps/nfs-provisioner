# Read originating documentation - IBM git repo and tutorial

# This is what I did...

1.	Decide which node will be the NFS server	=	jam4.local

2.	Provision storage on that server		=	/storage/dynamic
	root@jam4 dynamic]# ls -ld /storage/dynamic
	drwxr-xr-x. 2 root root 73 Sep 19 12:23 /storage/dynamic

3.	Download
	https://github.com/IBM/deploy-ibm-cloud-private/blob/master/extensions/nfs-class.yaml
	https://github.com/IBM/deploy-ibm-cloud-private/blob/master/extensions/nfs-deployment.yaml
	https://github.com/IBM/deploy-ibm-cloud-private/blob/master/extensions/nfs-test-claim.yaml

4.	Customise:
	nfs-deployment.yaml
	i)  Im not going to run this on the master so get rid of the "toletations" and target the specific worker/node = jam4.local
        ii) I changed name of the "provisioner" = scsuk.net

	nfs-provisioner $ diff webdocs/nfs-deployment.yaml nfs-deployment.yaml
	35,39d34
	<       tolerations:
	<       - key: "dedicated"
	<         operator: "Equal"
	<         value: "master"
	<         effect: "NoSchedule"
	41c36
	<         role: master
	---
	>         kubernetes.io/hostname: "jam4.local"
	61c56
	<             - "-provisioner=example.com/nfs"
	---
	>             - "-provisioner=scsuk.net/nfs"

5.	nfs-provisioner $ kubectl create -f nfs-deployment.yaml
	service/nfs-provisioner created
	deployment.extensions/nfs-provisioner created

6.	Customise storage class
	i) I just changed name of the provistioner to match the deployment

	nfs-provisioner $ diff webdocs/nfs-class.yaml nfs-class.yaml
	5c5
	< provisioner: example.com/nfs
	---
	> provisioner: scsuk.net/nfs

7.	Create the Storage Class
	
	nfs-provisioner $ kubectl create -f nfs-class.yaml
	storageclass.storage.k8s.io/nfs-dynamic created

	nfs-provisioner $ kubectl get StorageClass
	NAME          PROVISIONER     AGE
	nfs-dynamic   scsuk.net/nfs   23s

8.	Create a test claim

	nfs-provisioner $ kubectl create -f nfs-test-claim.yaml
	persistentvolumeclaim/nfs created
	nfs-provisioner $ kubectl describe pvc nfs
	Name:          nfs
	Namespace:     default
	StorageClass:  nfs-dynamic
	Status:        Bound
	Volume:        pvc-1373aee3-19f1-47bc-8816-4c0c35679af6
	Labels:        <none>
	Annotations:   control-plane.alpha.kubernetes.io/leader:
                 	{"holderIdentity":"01a90bd8-e368-11e9-9e9b-3ec7065c1e04","leaseDurationSeconds":15,"acquireTime":"2019-09-30T09:59:11Z","renewTime":"2019-...
               	pv.kubernetes.io/bind-completed: yes
               	pv.kubernetes.io/bound-by-controller: yes
               	volume.beta.kubernetes.io/storage-class: nfs-dynamic
               	volume.beta.kubernetes.io/storage-provisioner: scsuk.net/nfs
	Finalizers:    [kubernetes.io/pvc-protection]
	Capacity:      1Mi
	Access Modes:  RWX
	VolumeMode:    Filesystem
	Mounted By:    <none>
	Events:
  	Type    Reason                 Age                From                                                                                 Message
  	----    ------                 ----               ----                                                                                 -------
  	Normal  ExternalProvisioning   13s (x2 over 13s)  persistentvolume-controller                                                          waiting for a volume to be created, either by external provisioner "scsuk.net/nfs" or manually created by system administrator
  	Normal  Provisioning           13s                scsuk.net/nfs nfs-provisioner-7b9447dcfb-ssmrb 01a90bd8-e368-11e9-9e9b-3ec7065c1e04  External provisioner is provisioning volume for claim "default/nfs"
  	Normal  ProvisioningSucceeded  13s                scsuk.net/nfs nfs-provisioner-7b9447dcfb-ssmrb 01a90bd8-e368-11e9-9e9b-3ec7065c1e04  Successfully provisioned volume pvc-1373aee3-19f1-47bc-8816-4c0c35679af6
	nfs-provisioner $

	nfs-provisioner $ kubectl get pvc
	NAME   STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
	nfs    Bound    pvc-1373aee3-19f1-47bc-8816-4c0c35679af6   1Mi        RWX            nfs-dynamic    3m37s



	nfs-provisioner $ kubectl get pv
	NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM         STORAGECLASS   REASON   AGE
	pvc-1373aee3-19f1-47bc-8816-4c0c35679af6   1Mi        RWX            Delete           Bound    default/nfs   nfs-dynamic             3m40s


9.	See a directory has been created on the NFS server...  (see "Path")

	[root@jam4 dynamic]# ls -ld /storage/dynamic/pvc-1373aee3-19f1-47bc-8816-4c0c35679af6
	drwxrwsrwx. 2 root root 6 Sep 30 10:59 /storage/dynamic/pvc-1373aee3-19f1-47bc-8816-4c0c35679af6

	nfs-provisioner $ kubectl describe pv pvc-1373aee3-19f1-47bc-8816-4c0c35679af6
	Name:            pvc-1373aee3-19f1-47bc-8816-4c0c35679af6
	Labels:          <none>
	Annotations:     EXPORT_block:
	
                   	EXPORT
                   	{
                     	Export_Id = 1;
                     	Path = /export/pvc-1373aee3-19f1-47bc-8816-4c0c35679af6;
                     	Pseudo = /export/pvc-1373aee3-19f1-47bc-8816-4c0c35679af6;
                     	Access_Type = RW;
                     	Squash = no_root_squash;
                     	SecType = sys;
                     	Filesystem_id = 1.1;
                     	FSAL {
                       	Name = VFS;
                     	}
                   	}
                 	Export_Id: 1
                 	Project_Id: 0
                 	Project_block:
                 	Provisioner_Id: b8518a8d-d55d-11e9-bc16-56f89c2bbf74
                 	kubernetes.io/createdby: nfs-dynamic-provisioner
                 	pv.kubernetes.io/provisioned-by: scsuk.net/nfs
	Finalizers:      [kubernetes.io/pv-protection]
	StorageClass:    nfs-dynamic
	Status:          Bound
	Claim:           default/nfs
	Reclaim Policy:  Delete
	Access Modes:    RWX
	VolumeMode:      Filesystem
	Capacity:        1Mi
	Node Affinity:   <none>
	Message:
	Source:
    	Type:      NFS (an NFS mount that lasts the lifetime of a pod)
    	Server:    10.107.209.92
    	Path:      /export/pvc-1373aee3-19f1-47bc-8816-4c0c35679af6
    	ReadOnly:  false
	Events:        <none>

10.	Delete the PVC  ... and PV and correspondinig directory on NFS server is deleted....

	nfs-provisioner $ kubectl delete pvc nfs
	persistentvolumeclaim "nfs" deleted

	nfs-provisioner $ kubectl get pvc
	No resources found.

	nfs-provisioner $ kubectl get pv
	No resources found.

	[root@jam4 dynamic]# ls -ld /storage/dynamic/pvc-1373aee3-19f1-47bc-8816-4c0c35679af6
	ls: cannot access /storage/dynamic/pvc-1373aee3-19f1-47bc-8816-4c0c35679af6: No such file or director
